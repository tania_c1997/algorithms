package algorithms.algorithms;

public class SelectionSort {
	public static void SelectionSort ( int [ ] num ){
		int i,j;
		int n = num.length;
		
		/* advance the position through the entire array */
		for (j = 0; j < n-1; j++) {
		    /* assume the min is the first element */
		    int iMin = j;

		    /* test against elements after j to find the smallest */
		    for ( i = j+1; i < n; i++) {
				if (num[i] < num[iMin]) {
				       iMin = i;
			    }
		    }

		    if(iMin != j) {
		           int temp = num[iMin];
		           num[iMin] = num[j];
		           num[j] = temp;
		    }
		}
	}
}
