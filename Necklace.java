package algorithms;

import java.util.Scanner;

public class Necklace {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int k = sc.nextInt();
		int[] necklace = new int[n];
		int i;

		for (i = 0; i < k; i++)
			necklace[n - k + i] = sc.nextInt();
		for (i = 0; i < n - k; i++)
			necklace[i] = sc.nextInt();
		
	
		int max = 0;
		int currMax = 1;
		int last = necklace[0];
		for (i = 1; i< n; ++i) {
			if(necklace[i] == necklace[i-1]){
				currMax++;
			} else {
				currMax = 1;
			}
			
			if(currMax > max){
				max = currMax;
			}
		}
		
		System.out.print(max);
	}

}
