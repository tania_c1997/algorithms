package algorithms.algorithms;

public class QuickSort {
	private int[] quickSort(int lowerIndex, int higherIndex, int array[]) {
		
			int i = lowerIndex;
			int j = higherIndex;
			
			//calculate pivot number, I am taking pivot as the last number
			int pivot = array[array.length - 1];
			
			//Divide into two arrays
			while (i <= j) {
				while (array[i] < pivot) {
					i++;
				}
				
				while (array[j] > pivot) {
					j--;
				}
				
				if (i <= j) {
					swap(i, j);
					i++;
					j--;
				}
			}
			//call quickSort() method recursively
			if (lowerIndex < j) {
				quickSort(lowerIndex, j, array);
			}
			
			if (i < higherIndex) {
				quickSort(i, higherIndex, array);
			}
			
			return array;
	}

	private void swap(int i, int j) {
		int temp = i;
		i = j;
		j = temp;		
	}
	
}
