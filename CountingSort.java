package algorithms.algorithms;

public class CountingSort {
	public int[] countingSort(int[] theArray, int maxValue) {

		// array of 0s at indices 0..maxValue
		int numsToCounts[] = new int[maxValue + 1];

		// populate numsToCounts
		for (int num : theArray) {
			numsToCounts[num] += 1;
		}

		// populate the final sorted array
		int[] sortedArray = new int[theArray.length];
		int currentSortedIndex = 0;

		// for each num in numsToCounts
		for (int num = 0; num < numsToCounts.length; num++) {
			int count = numsToCounts[num];

		// for the number of times the item occurs
		for (int x = 0; x < count; x++) {
			// add it to the sorted array
			sortedArray[currentSortedIndex] = num;
			currentSortedIndex++;
		}
	}

	return sortedArray;
}

}
