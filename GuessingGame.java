package algorithms.algorithms;

import java.util.Scanner;

public class GuessingGame {
		public static void main(String [] args){
			
			Scanner sc = new Scanner(System.in);
			
			int minLimit = 0;
			int maxLimit = sc.nextInt();
			int number = sc.nextInt();
			sc.close();
			int guess = (minLimit + maxLimit)/2;
			int countGuesses = 1;		
			
			while(guess != number){

				if(guess > number){
					maxLimit = guess;
				}
				else{
					minLimit = guess;
				}
				
				guess = (minLimit + maxLimit)/2;
				countGuesses++;
			}
			
			System.out.print(countGuesses);
		}
}