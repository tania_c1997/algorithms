package algorithms.algorithms;

import java.util.Scanner;

public class Eliminations {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		
		int N = sc.nextInt();
		int Q = sc.nextInt();
		int [] eliminated = new int[N];
		
		for(int i = 0; i < N; i++){
			eliminated[i] = sc.nextInt();
		}
		int sum = 0;
		for(int i = 0; i < Q; i++){
			int numberEliminated = 0;
			int x = sc.nextInt();
			for(int j = 0; j < N; j++){
				if(eliminated[j] < x)
					numberEliminated++;
				if(eliminated[j] == x)
					numberEliminated = x;
			}
			sum += (x - numberEliminated);
		}
		System.out.println(sum);
	}
	
}
