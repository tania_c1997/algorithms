package algorithms.algorithms;

public class SieveOfErathostenes {
	private static void generatePrimes(int max) {
	    boolean[] isComposite = new boolean[max + 1];

	    for (int i = 2; i * i <= max; i++) {	
	        if (!isComposite [i]) {	            
	            //for every prime number, we mark all its 	multiples as composite numbers
	            for (int j = i; i * j <= max; j++) {
	                isComposite [i*j] = true;
	            }
	        }
	    }

	    for (int i = 2; i <= max; i++) {
	        if (!isComposite [i]) {
	        	System.out.print(i + " ");
	        }
	    }
	}

}
