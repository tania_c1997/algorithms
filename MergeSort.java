package algorithms.algorithms;

public class MergeSort {
	public static int[] mergeSort(int[] list) {

		// Split the array in half in two parts
		int[] first = new int[list.length / 2];
		int[] second = new int[list.length - first.length];

		System.arraycopy(list, 0, first, 0, first.length);
		System.arraycopy(list, first.length, second, 0, second.length);

		// Sort each half recursively
		mergeSort(first);
		mergeSort(second);

		// Merge both halves together, overwriting to original array
		merge(first, second, list);
		return list;
	}

	private static void  merge(int[] first, int[] second, int[] result) {
		int iFirst = 0;
		int iSecond = 0;
		int iMerged = 0;
	
		while (iFirst < first.length && iSecond < second.length) {
			if (first[iFirst] < second[iSecond]) {
				result[iMerged] = first[iFirst];
				iFirst++;
			} else {
				result[iMerged] = second[iSecond];
				iSecond++;
			}
			
			iMerged++;
		}
		//copy remaining elements from both halves - each half will have 	already sorted elements
		System.arraycopy (first, iFirst, result, iMerged, first.length - iFirst);
		System.arraycopy (second, iSecond, result, iMerged, second.length - iSecond);
	}
	
}