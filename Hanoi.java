package algorithms;

import java.util.Scanner;

public class Hanoi {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		Hanoi(N, 1, 2, 3);

	}

	private static void Hanoi(int n, int a, int b, int c) {
		if (n == 0)
			System.out.println(0+" ");
		else {
			if (n == 1) {
				System.out.print(a + "->" + b + " ");
			} else {
				Hanoi(n - 1, a, c, b);
				System.out.print(a + "->" + b + " ");
				Hanoi(n - 1, c, b, a);
			}
		}

	}
}
