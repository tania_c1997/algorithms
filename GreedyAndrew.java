package algorithms.algorithms;

import java.util.Arrays;
import java.util.Scanner;

public class GreedyAndrew {
	
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		
		int N = sc.nextInt();
		float G = sc.nextFloat();
		float maxWeight = 0;
		float maxCost = 0;
		float[] costs = new float [N];
		float[] weights = new float[N];
		float[] profits = new float[N];
		float[] sortedOnes = new float[N];
		
		for(int i = 0; i < N; i++){
			weights[i] = sc.nextFloat();
			costs[i] = sc.nextFloat();
			profits[i] = (float)(costs[i] / weights[i]);			
		}
		for(int i = 0; i < N; i++){
			sortedOnes[i] = profits[i];
		}
		
		Arrays.sort(sortedOnes);
				
		for(int i = N - 1; i >= 0; i--){
			int j;
			for(j = 0; j < N; j++){
				if(sortedOnes[i] == profits[j]){
					break;
				}
			}
			
			if(maxWeight + weights[j] <= G){
				maxWeight += weights[j];
				maxCost += costs[j];
				System.out.println(weights[j]);
			}
			else
				break;
		}
		System.out.println(maxCost);
	}

}
